#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
automx2_discover -- shortdesc

Copyright 2020 - [*] sys4 AG

Author: Klaus Tachtler <k@sys4.de>
        Patrick Ben Koetter <p@sys4.de>

automx2_discover.py is a test client to walk and show Autoconfig,
Autodiscover, mobileconfig settings as well as email related SRV Records.

  GNU GENERAL PUBLIC LICENSE
  Version 3, 29 June 2007

  Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
  Everyone is permitted to copy and distribute verbatim copies
  of this license document, but changing it is not allowed.

  https://www.gnu.org/licenses/gpl-3.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
  implied. See the License for the specific language governing
  permissions and limitations under the License.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.
'''

from __future__ import print_function

import sys
import os
import re
import argparse

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import urllib.request

import socket
import ssl

import dns.resolver

__all__ = []
__version__ = '0.1.dev6'
__date__ = '2019-12-05'
__updated__ = '2020-05-02'
__author__ = 'Klaus Tachtler <k@sys4.de>, Patrick Ben Koetter <p@sys4.de>'
__organisation__ = '[*] sys4 AG'

__DEBUG__ = False
__TESTRUN__ = False
__PROFILE__ = False

__charCountDebug__ = 40
__keyvalueFormatDebug__ = "{:39}: {:1}"

__charCount__ = 4
__keyvalueFormat__ = "{:3}: {:1}"


class EmailType(object):

    """
    Supports checking email against different patterns.
    The current available patterns is:
    RFC5322 (http://www.ietf.org/rfc/rfc5322.txt)
    """

    patterns = {
        'RFC5322':
            re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"),
    }

    def __init__(self, pattern):
        if pattern not in self.patterns:
            raise KeyError('{} is not a supported email pattern, choose from:'
                           ' {}'.format(pattern, ','.join(self.patterns)))
        self._rules = pattern
        self._pattern = self.patterns[pattern]

    def __call__(self, value):
        if not self._pattern.match(value):
            raise argparse.ArgumentTypeError(
                "'{}' is not a valid email address - does not match {} rules".format(
                    value, self._rules))
        return value


class DomainType(object):

    """
    Supports checking only the domain against different patterns.
    The current available patterns is:
    RFC5322 (http://www.ietf.org/rfc/rfc5322.txt) from email definition.
    """

    patterns = {
        'RFC5322': re.compile(r"^[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"),
    }

    def __init__(self, pattern):
        if pattern not in self.patterns:
            raise KeyError('{} is not a supported domain pattern, choose from:'
                           ' {}'.format(pattern, ','.join(self.patterns)))
        self._rules = pattern
        self._pattern = self.patterns[pattern]

    def __call__(self, value):
        if not self._pattern.match(value):
            raise argparse.ArgumentTypeError(
                "'{}' is not a valid domain - does not match {} rules".format(
                    value, self._rules))
        return value


class CLIError(Exception):

    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


def cli_parser(argv=None):  # IGNORE:C0111

    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = '''v %s
  Copyright (c) [*] sys4 AG. All Rights Reserved.
  Klaus Tachtler <k@sys4.de>
  Patrick Ben Koetter <p@sys4.de>
  https://sys4.de''' % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s (%s) %s' % (
        program_build_date, program_version)
    program_shortdesc = '%s\n%s\n%s' % (
        'automx2_discover.py is a test client to walk and show ',
        '  Autoconfig, Autodiscover, mobileconfig settings as well as email ',
        '  related SRV Records.')
    program_license = '''%s

  Created by %s.
  Copyright (c) %s. All rights reserved.
  Version: %s (%s).

  GNU GENERAL PUBLIC LICENSE
  Version 3, 29 June 2007

  Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
  Everyone is permitted to copy and distribute verbatim copies
  of this license document, but changing it is not allowed.

  https://www.gnu.org/licenses/gpl-3.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
  implied. See the License for the specific language governing
  permissions and limitations under the License.

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, __author__, __organisation__, str(__version__), str(__updated__))

    def exception_handler(exception_type,
                          exception,
                          traceback,
                          debug_hook=sys.excepthook):

        """Exception Handler manipulation"""

        if __DEBUG__:
            debug_hook(exception_type, exception, traceback)
        else:
            parser.print_usage()
            sys.stderr.write("%s: %s\n\n" % (
                exception_type.__name__, exception))
            sys.exit(9)

    sys.excepthook = exception_handler

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument(
            '-v',
            '--verbose',
            dest='verbose',
            action="count",
            help="set verbosity level [default: %(default)s]")
        parser.add_argument(
            '-V',
            '--version',
            action='version',
            version=program_version_message)
        discover = parser.add_argument_group('proceedings to discover')
        discover.add_argument(
            'EMAILADDRESS',
            action='store',
            type=EmailType('RFC5322'),
            default='',
            help=('Enter the to use email address.'))
        discover.add_argument(
            '--mozilla',
            action='store_true',
            default=False,
            required=False,
            help=('Just try the Mozilla autoconfig procedure and '
                  'show the results.'),
            dest='mozilla')
        discover.add_argument(
            '--microsoft',
            action='store_true',
            default=False,
            required=False,
            help=('Just try the Microsoft autodiscover procedure and '
                  'show the results.'),
            dest='microsoft')
        discover.add_argument(
            '--mobileconfig',
            action='store_true',
            default=False,
            required=False,
            help=('Just try the Apple mobileconfig procedure and '
                  'show the results.'),
            dest='apple')
        discover.add_argument(
            '--srv',
            action='store_true',
            default=False,
            required=False,
            help=('Just try the DNS based procedure and '
                  'show the results.'),
            dest='dns')
        settings = parser.add_argument_group('optional discover settings')
        settings.add_argument(
            '--no-ssl-validate',
            action='store_true',
            default=False,
            required=False,
            help=('No verification of the validity of the SSL Certificate.'),
            dest='nosslvalidate')
        settings.add_argument(
            '--no-color',
            action='store_true',
            default=False,
            required=False,
            help=('No colored output.'),
            dest='nocolor')

        # Process arguments
        args = parser.parse_args()

        # DEBUG mode was enabled.
        if __DEBUG__:

            print('=' * __charCountDebug__)
            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser", "start"))
            print('=' * __charCountDebug__)

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - verbose", str(args.verbose)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - EMAILADDRESS",
                str(args.EMAILADDRESS)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - --mozilla", str(args.mozilla)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - --mobileconfig", str(args.apple)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - --srv", str(args.dns)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - --no-ssl-validate",
                str(args.nosslvalidate)))

            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - --no-color",
                str(args.nocolor)))

        # Check argument EMAILADDRESS.
        if args.EMAILADDRESS is None:
            raise CLIError("Required argument - EMAILADDRESS - NOT set!")

        # Determine, which proceedings to discover was set.
        result = 0

        if args.mozilla:
            result += 2
        if args.microsoft:
            result += 4
        if args.apple:
            result += 8
        if args.dns:
            result += 16

        if __DEBUG__:
            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser - discover result", str(result)))

        # Check if one of the proceedings to discover are set,
        # if NOT set --all.
        if result <= 0:
            args.all = True
            result = 1

        # Check if more than one of the proceedings to discover are set.
        if (result != 1 and
                result != 2 and
                result != 4 and
                result != 8 and
                result != 16):
            errmsg = "Only one discover method could be set, NOT"

            if result > 16:
                errmsg += " --srv,"
                result -= 16

            if result >= 8:
                errmsg += " --mobileconfig,"
                result -= 8

            if result >= 4:
                errmsg += " --microsoft,"
                result -= 4

            if result >= 2:
                errmsg += " --mozilla,"
                result -= 2

            if result == 1:
                errmsg += " --all,"

            errmsg += " together!"

            raise CLIError(errmsg)

        if __DEBUG__:
            print('=' * __charCountDebug__)
            print(__keyvalueFormatDebug__.format(
                "DEBUG - cli_parser", "ended"))
            print('=' * __charCountDebug__)

        return args
    except KeyboardInterrupt:
        return None
    except RuntimeError as err:
        if __DEBUG__ or __TESTRUN__:
            raise err
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(err) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return None


def print_result(param, header, key, value, result):

    # https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
    """Print header or result string with or without color"""

    if bool(header):
        if param.nocolor:
            print(__keyvalueFormat__.format(
                "%s" % (str(key)), ""))
        else:
            print(__keyvalueFormat__.format(
                "\x1b[1;37;44m%s\x1b[0m" % (str(key)), ""))
    else:
        if bool(result):
            if param.nocolor:
                print(__keyvalueFormat__.format(
                    "%s" % (str(key)),
                    "[  OK  ] - %s" % (str(value))))
            else:
                print(__keyvalueFormat__.format(
                    "%s" % (str(key)),
                    "\x1b[0;37;42m[  OK  ]\x1b[0m - %s" % (str(value))))
        else:
            if param.nocolor:
                print(__keyvalueFormat__.format(
                    "%s" % (str(key)),
                    "[FAILED] - %s" % (str(value))))
            else:
                print(__keyvalueFormat__.format(
                    "%s" % (str(key)),
                    "\x1b[0;37;41m[FAILED]\x1b[0m - %s" % (str(value))))


def get_domain_part(param):

    """Determine the domain part of an email"""

    if __DEBUG__:
        print('=' * __charCountDebug__)
        print(__keyvalueFormatDebug__.format(
            "DEBUG - get_domain_part", "start"))
        print('=' * __charCountDebug__)

    param.domain = param.EMAILADDRESS.split('@')[1]

    if __DEBUG__:
        print(__keyvalueFormatDebug__.format(
            "DEBUG - get_domain_part - Domain", str(param.domain)))

    if __DEBUG__:
        print('=' * __charCountDebug__)
        print(__keyvalueFormatDebug__.format(
            "DEBUG - get_domain_part", "ended"))
        print('=' * __charCountDebug__)

    return param.domain


def get_dns_scheme(param, prefix):

    """Determine the dns scheme"""

    try:
        socket.gethostbyname('%s.%s' % (prefix, get_domain_part(param)))
        return True
    except socket.gaierror:
        return False


def request_url(param, url, rtype):

    """URL-Request"""

    if __DEBUG__:
        print(__keyvalueFormatDebug__.format(
            "DEBUG - request_url - url", str(url)))
        print(__keyvalueFormatDebug__.format(
            "DEBUG - request_url - rtype", str(rtype)))

    try:
        if rtype == 'MICROSOFT':
            data = (
                '<Autodiscover '
                'xmlns="http://schemas.microsoft.com'
                '/exchange/autodiscover/outlook/requestschema/2006">'
                '<Request><AcceptableResponseSchema>'
                'http://schemas.microsoft.com/'
                'exchange/autodiscover/outlook/responseschema/2006a'
                '</AcceptableResponseSchema>'
                '<EMailAddress>%s'
                '</EMailAddress></Request></Autodiscover>' % (param.EMAILADDRESS))
            request = urllib.request.Request(
                url,
                data=data.encode('utf-8'),
                headers={'Content-Type': 'application/xml'},
                method='POST')

            if param.nosslvalidate:
                find = urllib.request.urlopen(
                    request, context=ssl.SSLContext())
            else:
                find = urllib.request.urlopen(request)

            if param.verbose is not None:
                print(__keyvalueFormat__.format(
                    "  - POST-data", "%s" % (data)))

        elif rtype == 'MOZILLA':
            if param.nosslvalidate:
                find = urllib.request.urlopen(url, context=ssl.SSLContext())
            else:
                find = urllib.request.urlopen(url)
        elif rtype == 'APPLE':
            data = '_mobileconfig=true&cn=&emailaddress=%s&password=' % (param.EMAILADDRESS)
            request = urllib.request.Request(
                url,
                data=data.encode('utf-8'),
                method='POST')

            if param.nosslvalidate:
                find = urllib.request.urlopen(
                    request, context=ssl.SSLContext())
            else:
                find = urllib.request.urlopen(request)

            if param.verbose is not None:
                print(__keyvalueFormat__.format(
                    "  - POST-data", "%s" % (data)))

        else:
            if param.nosslvalidate:
                find = urllib.request.urlopen(url, context=ssl.SSLContext())
            else:
                find = urllib.request.urlopen(url)

        print_result(param, False, "Request URL", url, True)

        if param.verbose is not None:
            print(__keyvalueFormat__.format(find.read().decode('utf-8'), ""))

    except urllib.error.HTTPError as httperr:
        print_result(param, False, "Request URL", url, False)

        if param.verbose is not None:

            if rtype == 'MICROSOFT' or rtype == 'APPLE':
                print(__keyvalueFormat__.format(
                    "  - POST-data", "%s" % (data)))

            print(__keyvalueFormat__.format(
                "  - Error-Code, - Reason",
                "%s - %s" % (httperr.code, httperr.reason)))
            print(__keyvalueFormat__.format(
                "  - Error-Request-Header",
                "<<< start at next line >>> \n\n%s" % (httperr.headers)))
    except  urllib.error.URLError as urlerr:
        print_result(param, False, "Request URL", url, False)

        if param.verbose is not None:

            if rtype == 'MICROSOFT' or rtype == 'APPLE':
                print(__keyvalueFormat__.format(
                    "  - POST-data", "%s" % (data)))

            print(__keyvalueFormat__.format(
                "  - Error-Reason", "%s" % (urlerr.reason)))
    except ssl.CertificateError as certerr:
        print_result(param, False, "Request URL", url, False)

        if param.verbose is not None:
            print(__keyvalueFormat__.format(
                "  - CertificateError-Message", "%s" % (certerr)))


def request_dns(param, request):

    """DNS-Request"""

    if __DEBUG__:
        print(__keyvalueFormatDebug__.format(
            "DEBUG - request_dnsL - request", str(request)))

    try:
        answers = dns.resolver.query(request, 'SRV')
        print_result(param, False, "Request DNS", request, True)

        if param.verbose is not None:
            print(__keyvalueFormat__.format(
                "  - CanonicalName", "%s" % (answers.canonical_name)))
            print(__keyvalueFormat__.format(
                "  - Expiration", "%s" % (answers.expiration)))
            print(__keyvalueFormat__.format(
                "  - QName", "%s" % (answers.qname)))
            print(__keyvalueFormat__.format(
                "  - Response",
                "<<< start at next line >>>\n\n%s\n" % (answers.response)))
            print(__keyvalueFormat__.format(
                "  - RDClass", "%s" % (answers.rdclass)))
            print(__keyvalueFormat__.format(
                "  - RDType", "%s" % (answers.rdtype)))
            print(__keyvalueFormat__.format(
                "  - RRSet",
                "<<< start at next line >>>\n\n%s\n" % (answers.rrset)))

    except dns.resolver.NoAnswer as noanswer:
        print_result(param, False, "Request DNS", request, False)

        if param.verbose is not None:
            print(__keyvalueFormat__.format(
                "  - Error-Message", "%s" % (noanswer)))

    except dns.resolver.NXDOMAIN as notfound:
        print_result(param, False, "Request DNS", request, False)

        if param.verbose is not None:
            print(__keyvalueFormat__.format(
                "  - Error-Message", "%s" % (notfound)))


def mozilla(param):

    """Mozilla-Requests"""

    # Print headline.
    print_result(param, True, "mozilla", None, None)

    # https://autoconfig.thunderbird.net/v1.1/[DOMAINPART]
    request_url(param, 'https://autoconfig.thunderbird.net/v1.1/%s'
                % (get_domain_part(param)), 'MOZILLA')

    # Check if DNS record for autoconfig.__DOMAIN__ is available.
    if get_dns_scheme(param, 'autoconfig'):
        print(__keyvalueFormat__.format(
            "Search: DNS",
            "[FOUND ] - autoconfig.%s" % (get_domain_part(param))))

        # https://autoconfig.[DOMAINPART]/mail/config-v1.1.xml?emailaddress={EMAILADDRESS]
        request_url(param, 'https://autoconfig.%s/mail/config-v1.1.xml'
                    '?emailaddress=%s'
                    % (get_domain_part(param), param.EMAILADDRESS),
                    'MOZILLA')

    else:
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FAILED] - autoconfig.%s"
            % (get_domain_part(param))))

    # https://[DOMAINPART]/.well-known/autoconfig/mail/config-v1.1.xml
    request_url(param,
                'https://%s/.well-known/autoconfig/mail/config-v1.1.xml'
                % (get_domain_part(param)), 'MOZILLA')

    # http://autoconfig.thunderbird.net/v1.1/[DOMAINPART]
    request_url(param, 'http://autoconfig.thunderbird.net/v1.1/%s'
                % (get_domain_part(param)), 'MOZILLA')

    # Check if DNS record for autoconfig.__DOMAIN__ is available.
    if get_dns_scheme(param, 'autoconfig'):
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FOUND ] - autoconfig.%s"
            % (get_domain_part(param))))

        # http://autoconfig.[DOMAINPART]/mail/config-v1.1.xml?emailaddress={EMAILADDRESS]
        request_url(param, 'http://autoconfig.%s/mail/config-v1.1.xml?'
                    'emailaddress=%s' % (get_domain_part(param),
                                         param.EMAILADDRESS), 'MOZILLA')

    else:
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FAILED] - autoconfig.%s"
            % (get_domain_part(param))))

    # http://[DOMAINPART]/.well-known/autoconfig/mail/config-v1.1.xml
    request_url(param,
                'http://%s/.well-known/autoconfig/mail/config-v1.1.xml'
                % (get_domain_part(param)), 'MOZILLA')


def microsoft(param):

    """Microsoft-Requests"""

    # Print headline.
    print_result(param, True, "microsoft", None, None)

    # https://[DOMAINPART]/autodiscover/autodiscover.xml
    request_url(param, 'https://%s/autodiscover/autodiscover.xml'
                % (get_domain_part(param)), 'MICROSOFT')

    # Check if DNS record for autodiscover.__DOMAIN__ is available.
    if get_dns_scheme(param, 'autodiscover'):
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FOUND ] - autodiscover.%s"
            % (get_domain_part(param))))

        # https://autodiscover.[DOMAINPART]/autodiscover/autodiscover.xml
        request_url(param,
                    'https://autodiscover.%s/autodiscover/autodiscover.xml'
                    % (get_domain_part(param)), 'MICROSOFT')

        # http://autodiscover.[DOMAINPART]/autodiscover/autodiscover.xml
        request_url(param,
                    'http://autodiscover.%s/autodiscover/autodiscover.xml'
                    % (get_domain_part(param)),
                    'MICROSOFT')

    else:
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FAILED] - autodiscover.%s"
            % (get_domain_part(param))))

    # DNS: _autodiscover._tcp.[DOMAINPART]
    request_dns(param, '_autodiscover._tcp.%s' % (get_domain_part(param)))


def apple(param):

    """Apple-Requests (MobileConfig)"""

    # Print headline.
    print_result(param, True, "mobileconfig", None, None)

    # Check if DNS record for autodiscover.__DOMAIN__ is available.
    if get_dns_scheme(param, 'autodiscover'):
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FOUND ] - autodiscover.%s"
            % (get_domain_part(param))))

        # https://autodiscover.[DOMAINPART]/mobileconfig
        request_url(param, 'https://autodiscover.%s/mobileconfig'
                    % (get_domain_part(param)), 'APPLE')

    else:
        print(__keyvalueFormat__.format(
            "Search: DNS", "[FAILED] - autodiscover.%s"
            % (get_domain_part(param))))


def srv(param):

    """DNS-Requests"""

    # Print headline.
    print_result(param, True, "srv", None, None)

    # DNS: _submission._tcp.[DOMAINPART]
    request_dns(param, '_submission._tcp.%s' % (get_domain_part(param)))

    # DNS: _smtp._tcp.[DOMAINPART]
    request_dns(param, '_smtp._tcp.%s' % (get_domain_part(param)))

    # DNS: _imap._tcp.[DOMAINPART]
    request_dns(param, '_imap._tcp.%s' % (get_domain_part(param)))

    # DNS: _imaps._tcp.[DOMAINPART]
    request_dns(param, '_imaps._tcp.%s' % (get_domain_part(param)))

    # DNS: _pop3._tcp.[DOMAINPART]
    request_dns(param, '_pop3._tcp.%s' % (get_domain_part(param)))

    # DNS: _pop3s._tcp.[DOMAINPART]
    request_dns(param, '_pop3s._tcp.%s' % (get_domain_part(param)))


def main():

    """MAIN-Program"""

    arguments = cli_parser()

    if arguments is None:
        sys.stderr.write(
            "\nERROR:  An error has occurred at the cli_parser() "
            "and the program has been terminated!\n")
        sys.exit(9)

    if arguments.all:
        # --mozilla
        mozilla(arguments)
        # --microsoft
        microsoft(arguments)
        # --apple
        apple(arguments)
        # --srv
        srv(arguments)
    elif arguments.mozilla:
        # --mozilla
        mozilla(arguments)
    elif arguments.microsoft:
        # --microsoft
        microsoft(arguments)
    elif arguments.apple:
        # --apple
        apple(arguments)
    elif arguments.dns:
        # --srv
        srv(arguments)
    else:
        raise CLIError("NO discover method set!")


if __name__ == "__main__":

    if __TESTRUN__:
        import doctest
        doctest.testmod()
    if __PROFILE__:
        import cProfile
        import pstats
        PROFILE_FILENAME = 'automx2_discover_profile.bin'
        cProfile.run('main()', PROFILE_FILENAME)
        STATS_FILE = open("automx2_discover_profile_stats.txt", "w")
        PRINT = pstats.Stats(PROFILE_FILENAME, stream=STATS_FILE)
        STATS = PRINT.strip_dirs().sort_stats('cumulative')
        STATS.print_stats()
        STATS_FILE.close()
        sys.exit(0)

    sys.exit(main())
